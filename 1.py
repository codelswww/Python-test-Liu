#用for循环输出99乘法表1：

for i in range(1,10):#循环输出9列
    for j in range(1,i+1):#循环输出9行
        print ("{0:}*{1:}={2:<2}".format(j,i,i*j),end=" ")#输出算式
    print("")#每一行的回车
print("="*62)#分割线
#用while循环输出99乘法表1：
i=1
while i<10:
    j=1
    while j<i+1:
        print ("{0:}*{1:}={2:<2}".format(j,i,i*j),end=" ")
        j+=1
    i+=1
    print("")

#用for循环输出99乘法表2：
for i in range(9,0,-1):
    for j in range(1,i+1):
        print ("{0:}*{1:}={2:<2}".format(j,i,i*j),end=" ")
    print("")
print("="*62)

#用while循环输出99乘法表2：
i=9
while i>0:
    j=1
    while j<i+1:
        print ("{0:}*{1:}={2:<2}".format(j,i,i*j),end=" ")
        j+=1
    i-=1
    print("")



#用for循环输出99乘法表3：
for i in range(1,10):
    for j in range(8,i-1,-1):
        print("      ",end=" ")
    for j in range(i,0,-1):
        print ("{0:}*{1:}={2:<2}".format(j,i,i*j),end=" ")
    print("")
print("="*62)

#用while循环输出99乘法表3：
i=1
while i<10:
    j=1
    print("{0:>1} {1:>1} {2:>2} ".format(" "," ","  ")*(9-i),end="  ")
    while j<i+1:
        print ("{0:}*{1:}={2:<2} ".format(j,i,i*j),end="")
        j+=1
    i+=1
    print("")
print("="*62)



#用for循环输出99乘法表4：
for i in range(9,0,-1):
    for j in range(0,9-i):
        print("      ",end=" ")
    for j in range(i,0,-1):
        print ("{0:}*{1:}={2:<2}".format(j,i,i*j),end=" ")
    
    print("")
print("="*62)

#用while循环输出99乘法表4：
i=9
while i>0:
    j=1
    print("{0:>1} {1:>1} {2:>2} ".format(" "," ","  ")*(9-i),end="")
    while j<i+1:
        print ("{0:}*{1:}={2:<2}".format(j,i,i*j),end=" ")
        j+=1
    i-=1
    print("")
print("="*62)
